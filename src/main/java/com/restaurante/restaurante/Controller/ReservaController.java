package com.restaurante.restaurante.Controller;

import com.restaurante.restaurante.Models.ReservaModel;
import com.restaurante.restaurante.RestaurantApplication;
import com.restaurante.restaurante.Services.ReservaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/restaurante")

@CrossOrigin (origins ="*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE,
                                       RequestMethod.PATCH})
public class ReservaController {

    @Autowired
    ReservaService reservaService;

    @GetMapping("/reservas")
    public ResponseEntity<List<ReservaModel>> getReservas() {
        System.out.println("getReservas");

        return new ResponseEntity<>(
                this.reservaService.getReservas(),
                HttpStatus.OK
        );
    }

    @GetMapping("/reservas/{id}")
    public ResponseEntity<Object> getReservaById(@PathVariable int id) {
        System.out.println("getReservaById");
        System.out.println("La id de la reserva a buscar es " + id);

        Optional<ReservaModel> result = this.reservaService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Reserva no encontrada",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/reservas")
    public ResponseEntity<Object> addReserva(@RequestBody ReservaModel reserva) {
        System.out.println("addReserva");
        System.out.println("La id de la reserva a crear es " +reserva.getIdReserva());
        System.out.println("La fecha de la reserva a crear es " +reserva.getFechaReserva());
        System.out.println("La hora de la reserva a crear es " +reserva.getHoraReserva());
        System.out.println("El numero de comensales de la reserva a crear es " +reserva.getNumComensales());
        System.out.println("Comentarios de la reserva a crear son " +reserva.getComentariosAd());
        System.out.println("El nombre de la reserva a crear es " +reserva.getNombreReserva());

        boolean reservaEncontrada = false;
        boolean reservaDatosOk = false;

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

        if (reserva.getIdReserva() != 0  &&
            reserva.getFechaReserva() != null &&
            reserva.getHoraReserva() != null &&
            reserva.getNumComensales() != 0 &&
            reserva.getNombreReserva() != null) {
            reservaDatosOk = true;
        }

        if (reservaDatosOk) {
            if (reserva.getFechaReserva().compareTo(timeStamp.toString()) >= 0) {
                System.out.println("Fecha correcta reserva");
                for (ReservaModel reservaInList : RestaurantApplication.reservaModels) {
                    if (reservaInList.getIdReserva() == reserva.getIdReserva() &&
                            reservaInList.getFechaReserva().equals(reserva.getFechaReserva())) {
                        reservaEncontrada = true;
                    }
                }

                return new ResponseEntity<>(
                        reservaEncontrada ? "Ya tiene una reserva, entre por Modificación" : this.reservaService.add(reserva),
                        reservaEncontrada ? HttpStatus.FOUND : HttpStatus.CREATED
                );
            } else {
                return new ResponseEntity<>(
                        "No se puede reservar mesa en fecha pasada",
                        HttpStatus.BAD_REQUEST
                );
            }
        } else {
            return new ResponseEntity<>(
                    "Todos los datos de la reserva tienen que estar rellenos salvo comentarios",
                     HttpStatus.BAD_REQUEST
            );
        }
    }

    @DeleteMapping("/reservas/{id}")
    public ResponseEntity<String> deleteReserva(@PathVariable int id) {
        System.out.println("deleteReserva");
        System.out.println("La id de la reserva a borrar es " + id);

        boolean deleteReserva = this.reservaService.delete(id);

        return new ResponseEntity<> (
                deleteReserva ? "Reserva borrada" : "Reserva no encontrada" ,
                deleteReserva ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/reservas/{id}")
    public ResponseEntity<Object> updateReserva(@RequestBody ReservaModel reserva, @PathVariable int id) {
        System.out.println("updateReserva");
        System.out.println("La id de la reserva que se va a actualizar en parametro es " + id);
        System.out.println("La fecha de la reserva que se va a actualizar es " + reserva.getFechaReserva());
        System.out.println("La hora de la reserva que se va a actualizar es " + reserva.getHoraReserva());
        System.out.println("El numero de comensales de la reserva que se va a actualizar es " + reserva.getNumComensales());
        System.out.println("Los comentarios de la reserva que se va a actualizar son " + reserva.getComentariosAd());
        System.out.println("El nombre de la reserva que se va a actualizar es " + reserva.getNombreReserva());

        boolean reservaDatosOk = false;

        if (reserva.getIdReserva() != 0  &&
                reserva.getFechaReserva() != null &&
                reserva.getHoraReserva() != null &&
                reserva.getNumComensales() != 0 &&
                reserva.getNombreReserva() != null) {
            reservaDatosOk = true;
        }

        if(reservaDatosOk) {
            boolean updateReserva = this.reservaService.update(reserva);

            return new ResponseEntity<>(
                    updateReserva ? reserva : "Reserva no encontrada",
                    updateReserva ? HttpStatus.OK : HttpStatus.BAD_REQUEST
            );
        }else{
            return new ResponseEntity<>(
                    "Reserva no actualizada, introduzca todos los campos",
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    @PatchMapping("/reservas/{id}")
    public ResponseEntity<ReservaModel> updatePatchReserva(@RequestBody ReservaModel reserva, @PathVariable int id) {
        System.out.println("updatePatchReserva");
        System.out.println("La id de la reserva a actualizar es " + reserva.getIdReserva());
        System.out.println("La fecha de la reserva que se va a actualizar es " + reserva.getFechaReserva());
        System.out.println("La hora de la reserva que se va a actualizar es " + reserva.getHoraReserva());
        System.out.println("El numero de comensales de la reserva que se va a actualizar es " + reserva.getNumComensales());
        System.out.println("Los comentarios de la reserva que se va a actualizar son " + reserva.getComentariosAd());
        System.out.println("El nombre de la reserva que se va a actualizar es " + reserva.getNombreReserva());

        return new ResponseEntity<>(this.reservaService.patchUpdate(reserva), HttpStatus.OK);
    }
}