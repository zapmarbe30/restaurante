package com.restaurante.restaurante.Services;

import com.restaurante.restaurante.Models.ReservaModel;
import com.restaurante.restaurante.Repository.ReservaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservaService {

    @Autowired
    ReservaRepository reservaRepository;

    public List<ReservaModel> getReservas(){
        System.out.println("getReservas en ReservaService");

        return this.reservaRepository.findAll();
    }

    public Optional<ReservaModel> findById(int id){
        System.out.println("findById en ReservaService");

        return this.reservaRepository.findById(id);
    }

    public ReservaModel add(ReservaModel reserva) {
        System.out.println("add en ReservaService");

        return this.reservaRepository.save(reserva);
    }

    public boolean delete(int id){
        System.out.println("delete en ReservaService");

        boolean result = false;

        Optional<ReservaModel> reservaToDelete = this.findById(id);

        if (reservaToDelete.isPresent() == true) {
            result = true;
            this.reservaRepository.delete(reservaToDelete.get());
        }
        return result;
    }

    public boolean update(ReservaModel reserva){
        System.out.println("update en ReservaService");

        boolean result = false;

        Optional<ReservaModel> reservaToUpdate = this.findById(reserva.getIdReserva());

        if (reservaToUpdate.isPresent() == true) {
            result = true;
            this.reservaRepository.update(reservaToUpdate.get());
        }
        return result;
    }

    public ReservaModel patchUpdate(ReservaModel reservaModel){
        System.out.println("patchUpdate en ReservaService");

        return this.reservaRepository.patchUpdate(reservaModel);
    }

}