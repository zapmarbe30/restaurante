package com.restaurante.restaurante.Models;

public class ReservaModel {

    private int idReserva;
    private String fechaReserva;
    private String horaReserva;
    private int numComensales;
    private String comentariosAd;
    private String nombreReserva;

    public ReservaModel() {
    }

    public ReservaModel(int idReserva, String fechaReserva, String horaReserva, int numComensales, String comentariosAd, String nombreReserva) {
        this.idReserva = idReserva;
        this.fechaReserva = fechaReserva;
        this.horaReserva = horaReserva;
        this.numComensales = numComensales;
        this.comentariosAd = comentariosAd;
        this.nombreReserva = nombreReserva;
    }

    public int getIdReserva() {
        return this.idReserva;
    }

    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }

    public String getFechaReserva() {
        return this.fechaReserva;
    }

    public void setFechaReserva(String fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    public String getHoraReserva() {
        return this.horaReserva;
    }

    public void setHoraReserva(String horaReserva) {
        this.horaReserva = horaReserva;
    }

    public int getNumComensales() {
        return this.numComensales;
    }

    public void setNumComensales(int numComensales) {
        this.numComensales = numComensales;
    }

    public String getComentariosAd() {
        return this.comentariosAd;
    }

    public void setComentariosAd(String comentariosAd) {
        this.comentariosAd = comentariosAd;
    }

    public String getNombreReserva() {
        return this.nombreReserva;
    }

    public void setNombreReserva(String nombreReserva) {
        this.nombreReserva = nombreReserva;
    }
}
