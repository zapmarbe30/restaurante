package com.restaurante.restaurante.Repository;

import com.restaurante.restaurante.Models.ReservaModel;
import com.restaurante.restaurante.RestaurantApplication;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ReservaRepository {

    public List<ReservaModel> findAll(){
        System.out.println("findAll en ReservaRepository");

        return RestaurantApplication.reservaModels;
    }

    public Optional<ReservaModel> findById(int id) {
        System.out.println("findById en ReservaRepository");

        Optional<ReservaModel> result = Optional.empty();

        for (ReservaModel reservaInList : RestaurantApplication.reservaModels) {
            if (reservaInList.getIdReserva() == id) {
                System.out.println("Reserva encontrada idReserva " + id);
                result = Optional.of(reservaInList);
            }
        }
        return result;
    }

    public ReservaModel save(ReservaModel reserva) {
        System.out.println("save en ReservaRepository");

        RestaurantApplication.reservaModels.add(reserva);

        return reserva;
    }

    public void delete(ReservaModel reserva){
        System.out.println("delete en ReservaRepository");
        System.out.println("Borrando reserva");
        RestaurantApplication.reservaModels.remove(reserva);
    }

    public boolean update(ReservaModel reserva){
        System.out.println("update en ReservaRepository");

        boolean result = false;

        Optional <ReservaModel> reservaToUpdate = this.findById(reserva.getIdReserva());

        if (reservaToUpdate.isPresent() == true){
            System.out.println("Reserva para actualizar encontrada");

            ReservaModel reservaFromList = reservaToUpdate.get();

            reservaFromList.setFechaReserva(reserva.getFechaReserva());
            reservaFromList.setHoraReserva(reserva.getHoraReserva());
            reservaFromList.setNumComensales(reserva.getNumComensales());
            reservaFromList.setComentariosAd(reserva.getComentariosAd());
            reservaFromList.setNombreReserva(reserva.getNombreReserva());
            result = true;
        }

        return result;
    }

    public ReservaModel patchUpdate(ReservaModel reserva){
        System.out.println("update en ReservaRepository");

        Optional <ReservaModel> reservaToUpdate = this.findById(reserva.getIdReserva());

        ReservaModel patchReserva = reservaToUpdate.get();

        if (reservaToUpdate.isPresent() == true){
            System.out.println("Reserva para actualizar encontrada");

            if (reserva.getFechaReserva() != null && !reserva.getFechaReserva().equals(patchReserva.getFechaReserva())) {
                patchReserva.setFechaReserva(reserva.getFechaReserva());
                System.out.println("La fecha de la reserva actualizada es " + reserva.getFechaReserva());
            }

            if (reserva.getHoraReserva() != null && !reserva.getHoraReserva().equals(patchReserva.getHoraReserva())) {
                patchReserva.setHoraReserva(reserva.getHoraReserva());
                System.out.println("La hora de la reserva actualizada es " + reserva.getHoraReserva());
            }

            if (reserva.getNumComensales() != 0 && reserva.getNumComensales() != patchReserva.getNumComensales()) {
                patchReserva.setNumComensales(reserva.getNumComensales());
                System.out.println("El número de comensales de la reserva actualizada es " + reserva.getNumComensales());
            }

            if (reserva.getComentariosAd() != null && !reserva.getComentariosAd().equals(patchReserva.getComentariosAd())) {
                patchReserva.setComentariosAd(reserva.getComentariosAd());
                System.out.println("Los comentarios adicionales de la reserva actualizada son " + reserva.getComentariosAd());
            }

            if (reserva.getNombreReserva() != null && !reserva.getNombreReserva().equals(patchReserva.getNombreReserva())) {
                patchReserva.setNombreReserva(reserva.getNombreReserva());
                System.out.println("El nombre de la reserva actualizada es " + reserva.getNombreReserva());
            }
        }

        return patchReserva;
    }

}