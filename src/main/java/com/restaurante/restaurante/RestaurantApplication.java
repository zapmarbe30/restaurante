package com.restaurante.restaurante;

import com.restaurante.restaurante.Models.ReservaModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

@SpringBootApplication
public class RestaurantApplication {

	public static ArrayList<ReservaModel> reservaModels;

	public static void main(String[] args) {
		SpringApplication.run(RestaurantApplication.class, args);

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

		System.out.println(timeStamp);

		RestaurantApplication.reservaModels = RestaurantApplication.getReservasData();
	}

	private static ArrayList<ReservaModel> getReservasData(){

		ArrayList<ReservaModel> reservaModels = new ArrayList<>();

		reservaModels.add(
				new ReservaModel(
						650851201,
						"2021-10-30",
						"21:00",
						4,
						"Tenemos un vegano de esos raros",
						"Lara"
				)
		);
		reservaModels.add(
				new ReservaModel(
						699604374,
						"2021-10-31",
						"22:30",
						3,
						"Traemos un bebe por desgracia",
						"Javier"
				)
		);
		reservaModels.add(
				new ReservaModel(
						675355150,
						"2021-11-01",
						"21:00",
						10,
						"Necesitamos una trona para Belen",
						"Belen"
				)
		);

		return reservaModels;
	}

}